var express = require("express");
var bodyParser = require("body-parser");
var path = require("path");

// Loads sequelize ORM
var Sequelize = require("sequelize");

const NODE_PORT = process.env.NODE_PORT || 3000;
const CLIENT_FOLDER = path.join(__dirname, "../client");
//const IMG_FOLDER = path.join(__dirname, "../images");
//const MSG_FOLDER = path.join(__dirname, "../client/assets/messages");

const API_FRIENDS_ENDPOINT = "/api/friends";
const API_GROUPS_ENDPOINT = "/api/groups";

// Defines MySQL configuration
const MYSQL_USERNAME = 'root';
const MYSQL_PASSWORD = 'MyNewPass';

var app = express();

var sequelize = new Sequelize(
        'praticeDB',
        MYSQL_USERNAME,
        MYSQL_PASSWORD,
        {
            host: 'localhost',         // default port    : 3306
            logging: console.log,
            dialect: 'mysql',
            pool: {
                max: 5,
                min: 0,
                idle: 10000
            }
        }
    );

    // Loads model for department table
var testing = require('./models/testing')(sequelize, Sequelize);

app.use(express.static(CLIENT_FOLDER));
//app.use("/uploads", express.static(IMG_FOLDER));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));


// Routes
//require("./route")(app);
//require("./route")(app);

app.post("/test/test", function (req, res) {
    console.log("executed up to here");
    console.log("Received User Object",req.body);
    res.status(200).json(req.body.testInput1);


// This statement creates a record in the departments table. We are commenting this out becuase of a new
     // requirement where when a record is created in testing, an equivalent record must be written
     // Keeping this for future reference
     console.log("execution two");
     testing
     .create({
     name: req.body.testInput1.name,
     age: req.body.testInput1.age
    })
     .then(function (testInput1) {
     console.log(testInput1.get({plain: true}));
     res
     .status(200)
     .json(testInput1);
     })
     .catch(function (err) {
     console.log("error: " + err);
     res
     .status(500)
     .json(err);
     console.log("executed sequelize")
});

});

 

app.listen(NODE_PORT, function() {
    console.log("App started at port: %s", NODE_PORT);
});