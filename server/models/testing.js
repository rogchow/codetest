// Model for employees table
// References:
// http://docs.sequelizejs.com/en/latest/docs/getting-started/#your-first-model
// http://docs.sequelizejs.com/en/latest/docs/models-definition/
//Create a model for dept_manager table
module.exports = function(sequelize, Sequelize) {
    var testing =  sequelize.define('testing', {
        name: {
            type: Sequelize.STRING,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },

        age: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
    });

    return testing;

    }